import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardsComponent } from '../pages/cards.component';

/**
 * EXERCICE 12
 * Configurer les routes pour que le comportement soit similaire à celui que l'application avait quand tout était dans AppModule
 */
const routes: Routes = [
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardsRoutingModule {
}
