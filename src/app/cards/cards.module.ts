import { RouterModule } from '@angular/router';
import { CardsRoutingModule } from './cards.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { CardsComponent } from '../pages/cards.component';

/**
 * EXERCICE 12
 * Configurer correctement le module Cards pour qu'il regroupe tous les composants liés aux cartes
 */
@NgModule({
  declarations: [
    CardsComponent
  ],
  imports: [
    CommonModule,
    CardsRoutingModule,
    RouterModule,

    SharedModule
  ]
})
export class CardsModule { }
