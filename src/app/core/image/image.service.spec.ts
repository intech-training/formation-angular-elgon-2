import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DomSanitizer } from '@angular/platform-browser';
import { TestBed } from '@angular/core/testing';

import { ImageService } from './image.service';

describe('ImageService', () => {
  let imageService: ImageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule],
      providers: [
        ImageService,
        {
          provide: DomSanitizer,
          useValue: {
            bypassSecurityTrustStyle: (path: string) => `safeStyle: ${path}`
          }
        }
      ]
    });
    imageService = TestBed.get(ImageService);
});

  it('should be created', () => {
    expect(imageService).toBeTruthy();
  });
  it('should return the Safe Style for a img source', () => {
    const safeResourceUrl = 'http://test.lu/img.jpg';
    const sanitizedValue = `safeStyle: url(${safeResourceUrl})`;
    expect(imageService.getImageUrl('http://test.lu/img.jpg')).toBe(sanitizedValue);
  });
});
