import { AuthService } from '../auth/auth.service';
import {Injectable} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    const authToken = this.auth.getToken();
    const authReq = req.clone({ setHeaders: { Authorization: authToken } });

    return next.handle(authReq);
  }
}
