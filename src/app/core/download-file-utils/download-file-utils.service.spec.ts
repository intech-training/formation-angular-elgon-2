import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { DownloadFileUtilsService } from './download-file-utils.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('DownloadFileUtilsService', () => {
  let downloadFileUtilService: DownloadFileUtilsService;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [DownloadFileUtilsService]
  }));

  beforeEach(() => downloadFileUtilService = TestBed.get(DownloadFileUtilsService));

  it('should be created', () => {
    expect(downloadFileUtilService).toBeTruthy();
  });
});
