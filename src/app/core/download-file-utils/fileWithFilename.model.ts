

export interface FileWithFilename {
  blob: Blob;
  filename: string;
};
