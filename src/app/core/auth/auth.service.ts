import { Injectable } from '@angular/core';

/**
 * EXERCICE 13
 * Implémenter la fonction isLoggedIn dans le service AuthService. Elle doit retourner un booléen (toujours à true dans un premier temps).
 */
@Injectable()
export class AuthService {

  private FAKE_TOKEN = '1234';

  constructor() { }

  getToken(): string {

    return this.FAKE_TOKEN;
  }

  isLoggedIn(): boolean {
    throw new Error('not implemented');
  }

}
