import { CoreModule } from './../core.module';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';

/**
 * EXERCICE 13
 * Modifier le service AuthGuard, implémentant l’interface CanActivate, pour qu’il se serve de AuthService afin autoriser ou non l’accès à une route.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor() { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    throw new Error('not implemented');
  }


}
