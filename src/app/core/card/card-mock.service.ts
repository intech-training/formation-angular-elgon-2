import { Card } from './../../shared/models/card';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CardService } from './card.service';
import { Injectable } from '@angular/core';

/**
 * EXERCICE 06
 * Implémenter ce service pour qu'il retourne les mocks de carte au lieu des vraies réponses HTTP
 * Astuce : utiliser les mocks présents dans le fichier shared/card.mock.ts
 */
@Injectable()
export class CardMockService extends CardService {

  constructor(http: HttpClient) {
    super(http);
  }

  public getCards(): Observable<Card[]> {
    // TODO: to implement
    throw new Error('not implemented');
  }

  public getCard(id: string): Observable<Card> {
    // TODO: to implement
    throw new Error('not implemented');
  }
}
