import { Card } from './../../shared/models/card';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import { CardService } from './card.service';
import { environment } from 'src/environments/environment';

describe('CardService', () => {
  let service: CardService;
  let httpMock: HttpTestingController;

  const mockCards: Card[] = [
    {
      hp: 40,
      id: '1',
      imageUrl: 'https://images.pokemontcg.io/base1/58.png',
      imageUrlHiRes: 'https://images.pokemontcg.io/base1/58_hires.png',
      name: 'Test',
      nationalPokedexNumber: 1,
      number: 1,
      rarity: 'Common',
      subtype: 'Basic',
      supertype: 'Test',
      types: ['Fire']
    },
    {
      hp: 120,
      id: '2',
      imageUrl: 'https://images.pokemontcg.io/base1/57.png',
      imageUrlHiRes: 'https://images.pokemontcg.io/base1/57_hires.png',
      name: 'Other Test',
      nationalPokedexNumber: 2,
      number: 2,
      rarity: 'Common',
      subtype: 'Basic',
      supertype: 'Test',
      types: ['Leaf']
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CardService]
    });
    service = TestBed.get(CardService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => httpMock.verify());

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return an Observable<Card[]>', () => {
    service.getCards().subscribe(cards => {
      expect(cards.length).toBe(2);
      expect(cards).toEqual(mockCards);
    });

    const req = httpMock.expectOne(`${environment.apiUrl}/cards`);
    expect(req.request.method).toBe('GET');
    // On exécute manuellement la requête HTTP afin de finir le test
    req.flush(mockCards);
  });
});

