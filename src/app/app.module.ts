import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { CardSummaryComponent } from './components/card-summary/card-summary.component';
import { CoreModule } from './core/core.module';
import { AboutPage } from './pages/about/about.page';
import { CardsDetailPage } from './pages/cards-detail/cards-detail.page';
import { CardsListPage } from './pages/cards-list/cards-list.page';
import { CardsTradingFormPage } from './pages/cards-trading-form/cards-trading-form.page';
import { CardsTradingAddressFieldComponent } from './pages/cards-trading-form/components/cards-trading-address-field/cards-trading-address-field.component';
import { CardsComponent } from './pages/cards.component';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,

    CardsListPage,
    CardsDetailPage,
    CardSummaryComponent,
    AboutPage,
    CardsTradingAddressFieldComponent,
    CardsTradingFormPage,
    CardsComponent
  ],
  imports: [
    BrowserAnimationsModule,

    CoreModule.forRoot(),
    SharedModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
