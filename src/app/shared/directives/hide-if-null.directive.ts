
import {Directive, ElementRef, Input, Renderer2} from '@angular/core';

import * as _ from 'lodash';

/**
 * EXERCICE 04
 * Cette directive doit appliquer le style css `display: none` à l'élément sur laquelle elle est appliquée
 * si la valeur fournie est null ou undefined.
 */
@Directive({
  selector: '[hideIfNull]'
})
export class HideIfNullDirective {

  @Input() set hideIfNull(value: any) {
    // TODO: To implement
  }

  private processValue(value: any) {
    // TODO: To implement
  }

  constructor(private _el: ElementRef, private _renderer: Renderer2) {}

}
