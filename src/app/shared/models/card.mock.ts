import { Card } from './card';

export const mockCards: Card[] = [
  {
    hp: 40,
    id: '1',
    imageUrl: 'https://images.pokemontcg.io/base1/58.png',
    imageUrlHiRes: 'https://images.pokemontcg.io/base1/58_hires.png',
    name: 'Test',
    nationalPokedexNumber: 1,
    number: 1,
    rarity: 'Common',
    subtype: 'Basic',
    supertype: 'Test',
    types: ['Fire']
  },
  {
    hp: 120,
    id: '2',
    imageUrl: 'https://images.pokemontcg.io/base1/57.png',
    imageUrlHiRes: 'https://images.pokemontcg.io/base1/57_hires.png',
    name: 'Other Test',
    nationalPokedexNumber: 2,
    number: 2,
    rarity: 'Common',
    subtype: 'Basic',
    supertype: 'Test',
    types: ['Leaf']
  }
];
