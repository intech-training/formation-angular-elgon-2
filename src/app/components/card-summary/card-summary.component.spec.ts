import { async, TestBed } from '@angular/core/testing';

import { CardSummaryComponent } from './card-summary.component';

describe('CardSummaryComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSummaryComponent ]
    })
    .compileComponents();
  }));
});
