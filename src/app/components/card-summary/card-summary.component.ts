import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SafeStyle } from '@angular/platform-browser';

import { ImageService } from '../../core/image/image.service';
import { Card } from '../../shared/models/card';

@Component({
  selector: 'app-card-summary',
  templateUrl: './card-summary.component.html',
  styleUrls: ['./card-summary.component.scss']
})
export class CardSummaryComponent {
  @Input() card: Card;
  @Output() cardClicked: EventEmitter<Card> = new EventEmitter<Card>();

  constructor(private imageService: ImageService) { }

  public getCardImageStyle(source: string): SafeStyle {
    return this.imageService.getImageUrl(source);
  }

  public onCardClick(): void {
    this.cardClicked.emit(this.card);
  }

}
