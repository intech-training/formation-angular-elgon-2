import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';

import * as _ from 'lodash';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { CardService } from '../../core/card/card.service';
import { Trading } from '../../shared/models/trading';

/**
 * EXERCICE 05
 */
@Component({
  selector: 'app-cards-trading-form',
  templateUrl: './cards-trading-form.page.html',
  styleUrls: ['./cards-trading-form.page.scss']
})
export class CardsTradingFormPage implements OnInit {

  static isDateValid(control: FormControl): ValidationErrors {
    // TODO: to implement
    throw new Error('not implemented');
  }

  constructor(private formBuilder: FormBuilder,
              private cardService: CardService) { }

  ngOnInit() {
    // On crée le formulaire
    this.initForm();
  }

  handleForm(): void {
    // TODO: to implement
    // On gère la soumission du formulaire
  }

  private initForm(): void {
    // TODO: to implement
  }

  private isCardValid(control: AbstractControl): Observable<ValidationErrors> {
    // TODO: to implement
    throw new Error('not implemented');
  }

}
