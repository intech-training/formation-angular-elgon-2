import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsTradingFormPage } from './cards-trading-form.page';

describe('CardsTradingFormComponent', () => {
  let component: CardsTradingFormPage;
  let fixture: ComponentFixture<CardsTradingFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardsTradingFormPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsTradingFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
