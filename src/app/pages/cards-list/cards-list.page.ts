import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { CardService } from '../../core/card/card.service';
import { DownloadFileUtilsService } from '../../core/download-file-utils/download-file-utils.service';
import { Card } from '../../shared/models/card';

@Component({
  selector: 'app-cards-list',
  templateUrl: './cards-list.page.html',
  styleUrls: ['./cards-list.page.scss']
})
export class CardsListPage implements OnInit, OnDestroy {
  public cards: Card[] = [];
  // Multiple layout for switch
  public layouts: string[] = ['Card List', 'Table'];
  // Set default layout
  public currentLayout: string = this.layouts[0];

  public set sortOrder(value: 'asc' | 'desc') {
    // EXERCICE 08
    // TODO:
    // on navigue vers l'URL /cards en settant l'ordre de tri en query params
    // à l'aide du service Router
    if (value) {
      // On assigne la valeur récupérée dans un query param de l'URL
      // Puis on trie les cartes selon cet ordre
      this.sortCards(value);
    }
  }
  public get sortOrder(): 'asc' | 'desc' {
    // EXERCICE 08
    // Récupérer l'ordre de tri dans les query parameters
    return null;
  }

  private cardsSubscription: Subscription;
  private queryParamsSubscription: Subscription;

  constructor(private cardService: CardService,
    private downloadFileUtils: DownloadFileUtilsService,
    private router: Router,
    private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.loadAllCards();
  }

  ngOnDestroy(): void {
    if (this.cardsSubscription) {
      this.cardsSubscription.unsubscribe();
    }
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }
  }

  loadAllCards(): void {
    this.cardsSubscription = this.cardService.getCards().subscribe(cards => {
      this.cards = cards;

      // EXERCICE 09
      // Récupérer les query parameters pour pouvoir trier les cartes
      // De manière synchrone

      // EXERCICE 10
      // Idem mais de manière asynchrone
    });
  }

  downloadRulebook(): void {

    this.downloadFileUtils
      .getBinaryWithFilename(`${environment.apiUrl}/rulebook.pdf`)
      .pipe(
        map(fileWithFilename => {
          this.downloadFileUtils.downloadBinaryFromBlob(fileWithFilename.blob, fileWithFilename.filename);
        })
      )
      .subscribe();
  }

  openCardDetails(cardClicked: Card): void {
    // TODO: to implement
    // Utiliser le service Router pour naviguer vers les détails d'une carte
  }

  private sortCards(order: 'asc' | 'desc'): void {
    // TODO: to implement
    // EXERCICE 08
    // Ordonner la liste des cartes suivant l'ordre fourni
    // Utiliser la fonction orderBy de lodash
  }
}
