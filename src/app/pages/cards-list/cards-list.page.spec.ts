import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsListPage } from './cards-list.page';

describe('CardsListPage', () => {
  let component: CardsListPage;
  let fixture: ComponentFixture<CardsListPage>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [ CardsListPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsListPage);
    component = fixture.componentInstance;
    // trigger the change detection
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
