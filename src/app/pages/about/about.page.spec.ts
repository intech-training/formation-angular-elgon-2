import { CardsModule } from '../../cards.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutPage } from './about.page';

describe('AboutPage', () => {
  let component: AboutPage;
  let fixture: ComponentFixture<AboutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ CardsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    // On crée une fixture du composant
    fixture = TestBed.createComponent(AboutPage);
    // On récupère le composant de cette fixture
    component = fixture.componentInstance;
    // On lance la détection des changements, afin d'initialiser le composant
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
