import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsDetailPage } from './cards-detail.page';

describe('CardsDetailComponent', () => {
  let component: CardsDetailPage;
  let fixture: ComponentFixture<CardsDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardsDetailPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
