import { Component, OnInit } from '@angular/core';
import { SafeStyle } from '@angular/platform-browser';
import { ActivatedRoute, ParamMap, Params } from '@angular/router';

import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ImageService } from 'src/app/core/image/image.service';

import { CardService } from '../../core/card/card.service';
import { Card } from '../../shared/models/card';

@Component({
  selector: 'app-cards-detail',
  templateUrl: './cards-detail.page.html',
  styleUrls: ['./cards-detail.page.scss']
})
export class CardsDetailPage implements OnInit {
  public card: Card;
  public cardObservable: Observable<Card>;

  constructor(private imageService: ImageService,
              private cardService: CardService,
              private route: ActivatedRoute) { }



  ngOnInit() {
    // TODO: à implémenter
    // récupérer l'ID de la carte à afficher dans les paramètres de l'URL
    // Puis faire une requête vers le back-end pour récupérer les infos de la carte
  }

  getImage(source: string): SafeStyle {
    return this.imageService.getImageUrl(source);
  }

}
